﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    /// <summary>
    /// Custom exception class
    /// </summary>
    public class InvalidArmorException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public InvalidArmorException()
        {
        }

        /// <summary>
        /// Exception message overload
        /// </summary>
        public override string Message => "Wrong type of armor or hero level not high enough";
    }
}
