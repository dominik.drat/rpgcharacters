﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    /// <summary>
    /// Custom exception class
    /// </summary>
    public class InvalidWeaponException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public InvalidWeaponException()
        {
        }

        /// <summary>
        /// Exception message overload
        /// </summary>
        public override string Message => "Wrong type of the weapon or hero level not high enough";
    }
}
