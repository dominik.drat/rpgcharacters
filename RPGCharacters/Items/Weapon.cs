﻿using RPGCharacters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    /// <summary>
    /// Enum for type of weapon
    /// </summary>
    public enum WeaponType
    {
        WEAPON_AXE,
        WEAPON_BOW,
        WEAPON_DAGGER,
        WEAPON_HAMMER,
        WEAPON_STAFF,
        WEAPON_SWORD,
        WEAPON_WAND
    }

    /// <summary>
    /// Weapon class inheriting from Item class, it has weaponType and weaponAttributes fields
    /// </summary>
    public class Weapon : Item
    {
        private WeaponType weaponType;
        private WeaponAttributes weaponAttributes;
        public WeaponType WeaponType { get => weaponType; set => weaponType = value; }
        public WeaponAttributes WeaponAttributes { get => weaponAttributes; set => weaponAttributes = value; }
        
        /// <summary>
        /// Constructor
        /// </summary>
        public Weapon () : base() {
            WeaponAttributes = new WeaponAttributes();
        }

    }
}
