﻿using RPGCharacters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    /// <summary>
    /// Inventory class. Every hero has an inventory where armor pieces can be put when equipped
    /// 
    /// </summary>
    public class Inventory
    {
        private readonly Dictionary<Slot, Armor> itemsInventory;

        /// <summary>
        /// Constructor
        /// </summary>
        public Inventory()
        {
            itemsInventory = new Dictionary<Slot, Armor>();
        }

        /// <summary>
        /// A method putting armor to inventory
        /// </summary>
        /// <param name="armor">A piece of armor to be added to inventory</param>
        public void AddArmorToInventory (Armor armor)
        {
            itemsInventory.Add(armor.ItemSlot, armor);
        }

        /// <summary>
        /// A method for returning an inventory, so it can be used in other methods
        /// </summary>
        /// <returns></returns>
        public Dictionary<Slot, Armor> GetItemsInventory()
        {
            return itemsInventory;
        }
    }
}