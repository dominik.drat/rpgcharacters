﻿using RPGCharacters.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    /// <summary>
    /// Enum for types of armor
    /// </summary>
    public enum ArmorType
    {
        ARMOR_CLOTH,
        ARMOR_LEATHER,
        ARMOR_MAIL,
        ARMOR_PLATE
    }

    /// <summary>
    /// Armor class inheriting from Item class, contains two fields: armor type and primary attributes
    /// </summary>
    public class Armor : Item
    {
        private ArmorType armorType;
        private PrimaryAttributes attributes;

        public ArmorType ArmorType { get => armorType; set => armorType = value; }
        public PrimaryAttributes Attributes { get => attributes; set => attributes = value; }

        /// <summary>
        /// Constructor
        /// </summary>
        public Armor() : base()
        {

        }
    }
}
