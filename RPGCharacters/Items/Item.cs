﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Items
{
    /// <summary>
    /// Enum for item inventory slots
    /// </summary>
    public enum Slot
    {
        SLOT_WEAPON,
        SLOT_BODY,
        SLOT_HEAD,
        SLOT_LEGS
    }

    /// <summary>
    /// Item abstract class with itemName, itemLevel and itemSlot fields
    /// </summary>
    public abstract class Item
    {

        private string itemName;
        private int itemLevel;
        private Slot itemSlot;
        public string ItemName { get => itemName; set => itemName = value; }
        public int ItemLevel { get => itemLevel; set => itemLevel = value; }
        public Slot ItemSlot { get => itemSlot; set => itemSlot = value; }

    }
}
