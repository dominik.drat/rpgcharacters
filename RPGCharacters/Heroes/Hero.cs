﻿using RPGCharacters.Attributes;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Heroes
{
    /// <summary>
    /// Enum for hero class, used for automation when calculating damage of a hero
    /// </summary>
    public enum HeroClass
    {
        Warrior,
        Mage,
        Ranger,
        Rogue
    }
    /// <summary>
    /// An abstract class for a hero, includes fields, methods and abstract methods 
    /// </summary>
    public abstract class Hero
    {
        private string name;
        private int level;
        private HeroClass heroClass; 
        private PrimaryAttributes basePrimaryAttributes;
        private PrimaryAttributes totalPrimaryAttributes;
        private SecondaryAttributes secondaryAttributes;
        private Inventory inventory;
        private Weapon weapon;

        public string Name { get => name; set => name = value; }
        public int Level { get => level; set => level = value; }
        public HeroClass HeroClass { get => heroClass; set => heroClass = value; }
        public PrimaryAttributes BasePrimaryAttributes { get => basePrimaryAttributes; set => basePrimaryAttributes = value; }
        public PrimaryAttributes TotalPrimaryAttributes { get => totalPrimaryAttributes; set => totalPrimaryAttributes = value; }
        public SecondaryAttributes SecondaryAttributes { get => secondaryAttributes; set => secondaryAttributes = value; }
        public Inventory HeroInventory { get => inventory; set => inventory = value; }
        public Weapon HeroWeapon { get => weapon; set => weapon = value; }

        /// <summary>
        /// Constructor of the class with a parameter. Level of hero is always set to 1 when hero is created
        /// </summary>
        /// <param name="name">Name of the hero</param>
        public Hero (string name)
        {
            Name = name;
            Level = 1;
            BasePrimaryAttributes = new PrimaryAttributes();
            TotalPrimaryAttributes = new PrimaryAttributes();
            SecondaryAttributes = new SecondaryAttributes();
            HeroInventory = new Inventory();
            HeroWeapon = new Weapon()
            {
                // Creating dummy weapon ("Own hands") to avoid null exceptions when calculating hero damage
                ItemName = "Own hands",
                WeaponAttributes = new WeaponAttributes()
                {
                    AttackSpeed = 1,
                    Damage = 1
                }
            };
        }

        /// <summary>
        /// Abstract method for leveling up a hero
        /// </summary>
        /// <param name="levelsGranted">Amount of levels to be granted</param>
        public abstract void LevelUp(int levelsGranted);

        /// <summary>
        /// Abstract method for equipping a piece of armor
        /// </summary>
        /// <param name="armor">Armor to be equipped</param>
        /// <returns>Short message if equipping was successful or throws exception in case of failure</returns>
        public abstract string Equip(Armor armor);

        /// <summary>
        /// Abstract method for equipping a weapon
        /// </summary>
        /// <param name="weapon"></param>
        /// <returns>Short message if equipping was successful or exception in case of failure</returns>
        public abstract string Equip(Weapon weapon);

        /// <summary>
        /// A method for updating total primary attributes. Total attributes are calculated
        /// by adding base (hero) primary attributes together with primary attributes from armor pieces that are in inventory.
        /// 
        /// Inside the method, everytime total primary attributes are updated, secondary attributes are also updated by invoking
        /// CalculateSecondaryAttributes() and CalculateDamage() methods
        /// </summary>
        public void UpdateTotalAttributes()
        {
            PrimaryAttributes attributesFromInventory = AddAttributesFromInventory();
            TotalPrimaryAttributes = BasePrimaryAttributes + attributesFromInventory;
            CalculateSecondaryAttributes(); 
            CalculateDamage();
        }

        /// <summary>
        /// A method for summing up primary attributes from all armor pieces in inventory
        /// </summary>
        /// <returns>new PrimaryAttributes object, where primary attributes fields are sums of items primary attributes  </returns>
        public PrimaryAttributes AddAttributesFromInventory()
        {
            PrimaryAttributes attributesFromInventory = new();
            foreach (KeyValuePair <Slot, Armor> entry in HeroInventory.GetItemsInventory())
            {
                Armor temp = null;
                HeroInventory.GetItemsInventory().TryGetValue(entry.Key, out temp);
                attributesFromInventory += temp.Attributes;
            }
            return attributesFromInventory;
        }

        /// <summary>
        /// A method for calculating secondary hero attributes. 
        /// Takes total primary attributes (hero + items) into account
        /// </summary>
        public void CalculateSecondaryAttributes()
        {
            // Secondary stats calculated according to proper formulas
            SecondaryAttributes.ArmorRating = TotalPrimaryAttributes.Strength + TotalPrimaryAttributes.Dexterity;
            SecondaryAttributes.ElementalResistance = TotalPrimaryAttributes.Intelligence;
            SecondaryAttributes.Health = TotalPrimaryAttributes.Vitality * 10;
        }

        /// <summary>
        /// A method for calculating hero damage, which is based on weapon attributes and a class of hero
        /// </summary>
        public void CalculateDamage()
        {
            double primaryHeroAttribute = 0;
            double weaponDamagePerSecond = 1;

            // Taking weapon attributes into account
            weaponDamagePerSecond = HeroWeapon.WeaponAttributes.AttackSpeed * HeroWeapon.WeaponAttributes.Damage;

            // For each hero type, there is a different "main" attribute which is used in damage calculation formula
            switch (HeroClass)
            {
                case HeroClass.Warrior:
                    primaryHeroAttribute = TotalPrimaryAttributes.Strength;
                    break;
                case HeroClass.Rogue:
                    primaryHeroAttribute = TotalPrimaryAttributes.Dexterity;
                    break;
                case HeroClass.Ranger:
                    primaryHeroAttribute = TotalPrimaryAttributes.Dexterity;
                    break;
                case HeroClass.Mage:
                    primaryHeroAttribute = TotalPrimaryAttributes.Intelligence;
                    break;
            }
            // The actual hero damage formula
            SecondaryAttributes.DamagePerSecond = weaponDamagePerSecond * (1 + (primaryHeroAttribute / 100));
        }

        /// <summary>
        /// A method for displaying hero information
        /// </summary>
        public void DisplayStatistics()
        {
            StringBuilder sb = new();
            sb.AppendLine($"Statistics of your character:");
            sb.AppendLine($"Name: {Name}");
            sb.AppendLine($"Level {Level} {HeroClass}");
            sb.AppendLine($"Strength: {TotalPrimaryAttributes.Strength}");
            sb.AppendLine($"Dexterity: {TotalPrimaryAttributes.Dexterity}");
            sb.AppendLine($"Intelligence: {TotalPrimaryAttributes.Intelligence}");
            sb.AppendLine($"Health: {SecondaryAttributes.Health}");
            sb.AppendLine($"Armor rating: {SecondaryAttributes.ArmorRating}");
            sb.AppendLine($"Dexterity: {SecondaryAttributes.ElementalResistance}");
            sb.AppendLine($"DPS: {SecondaryAttributes.DamagePerSecond}");
            Console.WriteLine(sb.ToString());
        }
    }
}
