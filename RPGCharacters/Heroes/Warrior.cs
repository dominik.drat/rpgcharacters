﻿using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Heroes
{
    public class Warrior : Hero
    {
        /// <summary>
        /// Warrior class constructor. Proper base primary attributes are assigned, UpdateTotalAttributes method is invoked
        /// </summary>
        /// <param name="name">The name of the hero</param>
        public Warrior(string name) : base(name)
        {
            HeroClass = HeroClass.Warrior;

            BasePrimaryAttributes.Strength = 5;
            BasePrimaryAttributes.Dexterity = 2;
            BasePrimaryAttributes.Intelligence = 1;
            BasePrimaryAttributes.Vitality = 10;

            UpdateTotalAttributes();
        }

        /// <summary>
        /// A method for leveling up the hero. Increases hero base primary attributes by
        /// a set of values different for each class, multiplied by parameter
        /// </summary>
        /// <param name="levelsGranted">Amount of levels to be granted</param>
        public override void LevelUp(int levelsGranted)
        {
            if (levelsGranted < 1)
            {
                throw new ArgumentException("Amount of levels granted has to be greater than 0");
            }
            else
            {
                BasePrimaryAttributes.Strength += (levelsGranted * 3);
                BasePrimaryAttributes.Dexterity += (levelsGranted * 2);
                BasePrimaryAttributes.Intelligence += (levelsGranted * 1);
                BasePrimaryAttributes.Vitality += (levelsGranted * 5);

                Level += levelsGranted;
           
                UpdateTotalAttributes();
            }
        }

        /// <summary>
        /// A method for equipping a piece of armor. Item is elgible for equipping if its level
        /// is not higher than hero level. Armor cannot be equipped if it has wrong armor type
        /// (that means a hero of specific type cannot wear that type of armor).
        /// 
        /// If equipping is successful, a method for calculating new total primary attributes is invoked
        /// </summary>
        /// <param name="armor">Armor to be equipped</param>
        /// <returns>Short message if equipping was successful or throws exception in case of failure</returns>
        public override string Equip(Armor armor)
        {
            string message = "";
            if (armor.ItemLevel > Level ||
                armor.ArmorType == ArmorType.ARMOR_CLOTH ||
                armor.ArmorType == ArmorType.ARMOR_LEATHER)
            {
                throw new InvalidArmorException();
            }
            else
            {
                HeroInventory.AddArmorToInventory(armor);
                message = "New armor equipped!";
                UpdateTotalAttributes();
            }
            return message;
        }

        /// <summary>
        /// A method for equipping a weapon. Item is elgible for equipping if its level
        /// is not higher than hero level. Weapon cannot be equipped if it has wrong weapon type
        /// (that means a hero of specific type cannot wear that type of weapon).
        /// </summary>
        /// <param name="weapon">Weapon to be equipped</param>
        /// <returns>Short message if equipping was successful or throws exception in case of failure</returns>
        public override string Equip(Weapon weapon)
        {
            string message = "";
            if (weapon.ItemLevel <= Level && 
                (weapon.WeaponType == WeaponType.WEAPON_AXE ||
                    weapon.WeaponType == WeaponType.WEAPON_SWORD ||
                    weapon.WeaponType == WeaponType.WEAPON_HAMMER))
            {
                HeroWeapon = weapon;
                message = "New weapon equipped!";
                CalculateDamage();
            }
            else
            {
                throw new InvalidWeaponException();
            }
            return message;
        }
    }
}
