﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attributes
{
    /// <summary>
    /// A class for storing hero secondary attributes
    /// </summary>
    public class SecondaryAttributes
    {
        private int health;
        private int armorRating;
        private int elementalResistance;
        private double damagePerSecond;

        public int Health { get => health; set => health = value; }
        public int ArmorRating { get => armorRating; set => armorRating = value; }
        public int ElementalResistance { get => elementalResistance; set => elementalResistance = value; }
        public double DamagePerSecond { get => damagePerSecond; set => damagePerSecond = value; }

        /// <summary>
        /// Method override for Equals(), used in tests project for secondary attributes comparison
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>True or false</returns>
        public override bool Equals(object obj)
        {
            return obj is SecondaryAttributes secondaryAttributes &&
                Health == secondaryAttributes.Health &&
                ArmorRating == secondaryAttributes.ArmorRating &&
                ElementalResistance == secondaryAttributes.ElementalResistance;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(Health, ArmorRating, ElementalResistance);
        }
    }
}
