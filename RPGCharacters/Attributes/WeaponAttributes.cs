﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attributes
{
    /// <summary>
    /// A class for storing weapon attributes
    /// </summary>
    public class WeaponAttributes
    {
        private int damage;
        private double attackSpeed;

        public int Damage { get => damage; set => damage = value; }
        public double AttackSpeed { get => attackSpeed; set => attackSpeed = value; }
    }
}
