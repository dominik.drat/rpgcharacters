﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Attributes
{
    /// <summary>
    /// A class for storing primary hero and armor attributes
    /// </summary>
    public class PrimaryAttributes
    {
        private int strength;
        private int dexterity;
        private int intelligence;
        private int vitality;

        public int Strength { get => strength; set => strength = value;  }
        public int Dexterity { get => dexterity; set => dexterity = value;  }
        public int Intelligence { get => intelligence; set => intelligence = value;  }
        public int Vitality { get => vitality; set => vitality = value;  }

        /// <summary>
        /// A method for overloading the + operator 
        /// </summary>
        /// <param name="firstSet">First set of primary attributes</param>
        /// <param name="secondSet">Second set of primary attributes</param>
        /// <returns>A new set of primary attributes, where values are summed up from firstSet and secondSet</returns>
        public static PrimaryAttributes operator +(PrimaryAttributes firstSet, PrimaryAttributes secondSet)
        {
            return new PrimaryAttributes
            {
                Strength = firstSet.Strength + secondSet.Strength,
                Dexterity = firstSet.Dexterity + secondSet.Dexterity,
                Intelligence = firstSet.Intelligence + secondSet.Intelligence,
                Vitality = firstSet.Vitality + secondSet.Vitality
            };
        }
        /// <summary>
        /// Method override for Equals(), used in tests project for primary attributes comparison
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>True or false</returns>
        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes primaryAttributes &&
                Strength == primaryAttributes.Strength &&
                Dexterity == primaryAttributes.Dexterity &&
                Intelligence == primaryAttributes.Intelligence &&
                Vitality == primaryAttributes.Vitality;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence, Vitality);
        }
    }
}
