﻿using RPGCharacters.Attributes;
using RPGCharacters.Heroes;
using RPGCharacters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPGCharactersTests
{
    /// <summary>
    /// Tests which relate to equipping items and resulting attributes changes 
    /// Some tests also cover restrictions which prevent specific types of heroes
    /// from equipping armor/weapon of specific type
    /// </summary>
    public class ItemsEquipmentTests
    {
        [Fact]
        public void EquipWeapon_WarriorEquippingHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Warrior testWarrior = new("testWarrior");
            Weapon testAxe = new()
            {
                ItemName = "Common axe",
                ItemLevel = 2,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            // Act + Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.Equip(testAxe));
        }

        [Fact]
        public void EquipArmor_WarriorEquippingHighLevelArmor_ShouldThrowInvalidArmorException()
        {
            // Arrange
            Warrior testWarrior = new("testWarrior");
            Armor testPlateBody = new()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            // Act + Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.Equip(testPlateBody));
        }

        [Fact]
        public void EquipWeapon_WarriorEquippingWrongWeaponType_ShouldThrowInvalidWeaponException()
        {
            // Arrange
            Warrior testWarrior = new("testWarrior");
            Weapon testBow = new()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };
            // Act + Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.Equip(testBow));
        }

        [Fact]
        public void EquipArmor_WarriorEquippingWrongArmorType_ShouldThrowInvalidArmorException()
        {
            // Arrange
            Warrior testWarrior = new("testWarrior");
            Armor testClothHead = new()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_HEAD,
                ArmorType = ArmorType.ARMOR_CLOTH,
                Attributes = new PrimaryAttributes() { Vitality = 1, Intelligence = 5 }
            };
            // Act + Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.Equip(testClothHead));
        }

        [Fact]
        public void EquipWeapon_WarriorEquippingValidWeapon_ShouldReturnSuccessMessage()
        {
            // Arrange
            string expected = "New weapon equipped!";
            Warrior testWarrior = new("testWarrior");
            Weapon testAxe = new()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            // Act
            string actual = testWarrior.Equip(testAxe);


            // Assert
            Assert.Equal(expected,actual);
        }

        [Fact]
        public void EquipArmor_WarriorEquippingValidArmor_ShouldReturnSuccessMessage()
        {
            // Arrange
            string expected = "New armor equipped!";
            Warrior testWarrior = new("testWarrior");
            Armor testPlateBody = new()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            // Act
            string actual = testWarrior.Equip(testPlateBody);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_WarriorEquippingValidArmor_WarriorTotalAttributesShouldIncreaseProperly()
        {
            // Aarrange
            Warrior testWarrior = new("testWarrior");
            Armor testPlateBody = new()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            PrimaryAttributes expected = new()
            {
                Strength = 6,
                Dexterity = 2,
                Intelligence = 1,
                Vitality = 12
            };
            // Act
            testWarrior.Equip(testPlateBody);
            PrimaryAttributes actual = testWarrior.TotalPrimaryAttributes;
            Assert.True(expected.Equals(actual));
        }

        [Fact]
        public void CalculateDamage_WarriorWithNoWeaponEquipped_WarriorDamageShouldBeCalculatedCorrectly()
        {
            // Arrange
            double expected = 1 * (1 + (5.0 / 100));
            // Act
            Warrior testWarrior = new("testWarrior");
            double actual = testWarrior.SecondaryAttributes.DamagePerSecond;
            // Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CalculateDamage_WarriorEquippingWeapon_WarriorDamageShouldBeCalculatedCorrectly()
        {
            // Arrange
            double expected = (7 * 1.1) * (1 + (5.0 / 100));
            Warrior testWarrior = new("testWarrior");
            Weapon testAxe = new()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            // Act
            testWarrior.Equip(testAxe);
            double actual = testWarrior.SecondaryAttributes.DamagePerSecond;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDamage_WarriorEquippingWeaponAndArmor_WarriorDamageShouldBeCalculatedCorrectly()
        {
            // Arrange
            double expected = (7 * 1.1) * (1 + ((5.0 + 1) / 100));
            Warrior testWarrior = new("testWarrior");
            Weapon testAxe = new()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_WEAPON,
                WeaponType = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor testPlateBody = new()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.SLOT_BODY,
                ArmorType = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Vitality = 2, Strength = 1 }
            };
            // Act
            testWarrior.Equip(testAxe);
            testWarrior.Equip(testPlateBody);
            double actual = testWarrior.SecondaryAttributes.DamagePerSecond;
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
