using RPGCharacters.Attributes;
using RPGCharacters.Heroes;
using System;
using Xunit;

namespace RPGCharactersTests
{
    /// <summary>
    /// Tests related to coretness of level after creating hero and when leveling up a hero
    /// </summary>
    public class HeroTests
    {
        [Fact]
        public void Constructor_CreatingHero_HeroShouldHaveLevelOneAfterCreation()
        {
            // Arrange
            Warrior testWarrior = new("testWarrior");
            int expected = 1;
            // Act
            int actual = testWarrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_HeroLevelupByOne_HeroShouldBeLevelTwo()
        {
            // Arrange
            Warrior testWarrior = new("testWarrior");
            testWarrior.LevelUp(1);
            int expected = 2;
            // Act
            int actual = testWarrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData (0)]
        [InlineData (-1)]
        public void LevelUp_HeroLevelUpByNonPositiveNumbers_ShouldThrowArgumentException(int number)
        {
            // Arrange
            Warrior testWarrior = new("testWarrior");
            // Act + Assert
            Assert.Throws<ArgumentException>(() => testWarrior.LevelUp(number));
        }
    }
}
