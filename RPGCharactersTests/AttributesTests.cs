﻿using RPGCharacters.Attributes;
using RPGCharacters.Heroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPGCharactersTests
{
    /// <summary>
    /// Tests related to primary and secondary attributes
    /// </summary>
    public class AttributesTests
    {
        #region Creation Attributes Tests
        [Fact]
        public void Warrior_CreatingWarrior_WarriorShouldHaveProperBaseAttributes()
        {
            // Arrange
            
            PrimaryAttributes expectedPrimaryAttributes = new()
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
                Vitality = 10,
            };
            // Act
            Warrior testWarrior = new("testWarrior");
            PrimaryAttributes actualPrimaryAttributes = testWarrior.BasePrimaryAttributes;
            // Assert
            Assert.True(expectedPrimaryAttributes.Equals(actualPrimaryAttributes));
        }

        [Fact]
        public void Mage_CreatingMage_MageShouldHaveProperBaseAttributes()
        {
            // Arrange
            PrimaryAttributes expectedPrimaryAttributes = new()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
                Vitality = 5,
            };
            // Act
            Mage testMage = new("testMage");
            PrimaryAttributes actualPrimaryAttributes = testMage.BasePrimaryAttributes;
            // Assert
            Assert.True(expectedPrimaryAttributes.Equals(actualPrimaryAttributes));
        }

        [Fact]
        public void Rogue_CreatingRogue_RogueShouldHaveProperBaseAttributes()
        {
            // Arrange
            PrimaryAttributes expectedPrimaryAttributes = new()
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
                Vitality = 8,
            };
            // Act
            Rogue testRogue = new("testRogue");
            PrimaryAttributes actualPrimaryAttributes = testRogue.BasePrimaryAttributes;
            // Assert
            Assert.True(expectedPrimaryAttributes.Equals(actualPrimaryAttributes));
        }

        [Fact]
        public void Ranger_CreatingRanger_RangerShouldHaveProperBaseAttributes()
        {
            // Arrange
            PrimaryAttributes expectedPrimaryAttributes = new()
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1,
                Vitality = 8,
            };
            // Act
            Ranger testRanger = new("testRanger");
            PrimaryAttributes actualPrimaryAttributes = testRanger.BasePrimaryAttributes;
            // Assert
            Assert.True(expectedPrimaryAttributes.Equals(actualPrimaryAttributes));
        }
        #endregion

        #region Attributes after LevelUp(1)
        [Fact]
        public void LevelUp_LevelingUpWarrior_WarriorShouldHaveProperBaseAttributes()
        {
            // Arrange
            Warrior warrior = new("testWarrior");
            Warrior testWarrior = warrior;
            PrimaryAttributes expectedPrimaryAttributes = new()
            {
                Strength = 8,
                Dexterity = 4,
                Intelligence = 2,
                Vitality = 15,
            };
            // Act
            testWarrior.LevelUp(1);
            PrimaryAttributes actualPrimaryAttributes = testWarrior.BasePrimaryAttributes;
            // Assert
            Assert.True(expectedPrimaryAttributes.Equals(actualPrimaryAttributes));
        }

        [Fact]
        public void LevelUp_LevelingUpMage_MageShouldHaveProperBaseAttributes()
        {
            // Arrange
            Mage testMage = new("testMage");
            PrimaryAttributes expectedPrimaryAttributes = new()
            {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13,
                Vitality = 8,
            };
            // Act
            testMage.LevelUp(1);
            PrimaryAttributes actualPrimaryAttributes = testMage.BasePrimaryAttributes;
            // Assert
            Assert.True(expectedPrimaryAttributes.Equals(actualPrimaryAttributes));
        }

        [Fact]
        public void LevelUp_LevelingUpRogue_RogueShouldHaveProperBaseAttributes()
        {
            // Arrange
            Rogue testRogue = new("testRogue");
            PrimaryAttributes expectedPrimaryAttributes = new()
            {
                Strength = 3,
                Dexterity = 10,
                Intelligence = 2,
                Vitality = 11,
            };
            // Act
            testRogue.LevelUp(1);
            PrimaryAttributes actualPrimaryAttributes = testRogue.BasePrimaryAttributes;
            // Assert
            Assert.True(expectedPrimaryAttributes.Equals(actualPrimaryAttributes));
        }

        [Fact]
        public void LevelUp_LevelingUpRanger_RangerShouldHaveProperBaseAttributes()
        {
            // Arrange
            Ranger testRanger = new("testRanger");
            
            PrimaryAttributes expectedPrimaryAttributes = new()
            {
                Strength = 2,
                Dexterity = 12,
                Intelligence = 2,
                Vitality = 10,
            };
            // Act
            testRanger.LevelUp(1);
            PrimaryAttributes actualPrimaryAttributes = testRanger.BasePrimaryAttributes;
            // Assert
            Assert.True(expectedPrimaryAttributes.Equals(actualPrimaryAttributes));
        }
        #endregion

        [Fact]
        public void LevelUp_LevelingUpWarrior_WarriorShouldHaveProperSecondaryAttributes()
        {
            // Arrange
            Warrior testWarrior = new("testWarrior");
            SecondaryAttributes expectedSecondaryAttributes = new()
            {
                Health = 150,
                ArmorRating = 12,
                ElementalResistance = 2
            };
            // Act
            testWarrior.LevelUp(1);
            SecondaryAttributes actualSecondaryAttributes = testWarrior.SecondaryAttributes;
            // Assert
            Assert.True(expectedSecondaryAttributes.Equals(actualSecondaryAttributes));
        }
    }
}
